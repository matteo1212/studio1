package com.testunitari.testunitari;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public abstract class BaseService {

    @Autowired
    protected ModelMapper mapper;

    @Autowired
    protected ApplicationContext context;
}
