package com.testunitari.testunitari.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PersonaDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPersona;

    private String nome;

    private String cognome;

    private String codiceFiscale;

    private Date datNascita;

    private List<AutomobileDto> automobili;

    public PersonaDto setFluentIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
        return this;
    }
}
