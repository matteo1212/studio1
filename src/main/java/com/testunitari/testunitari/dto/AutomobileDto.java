package com.testunitari.testunitari.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AutomobileDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idAutomobile;

    private String carburante;

    private String marca;

    private String modello;

    private String anno;

    private PersonaDto persona;

}
