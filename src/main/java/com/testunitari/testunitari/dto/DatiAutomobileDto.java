package com.testunitari.testunitari.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DatiAutomobileDto extends AutomobileDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private PersonaDto persona;

}
