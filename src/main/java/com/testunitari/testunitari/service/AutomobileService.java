package com.testunitari.testunitari.service;

import com.testunitari.testunitari.BaseService;
import com.testunitari.testunitari.dto.AutomobileDto;
import com.testunitari.testunitari.dto.DatiAutomobileDto;
import com.testunitari.testunitari.entity.Automobile;
import com.testunitari.testunitari.repositorio.AutomobileRepositorio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@SuppressWarnings(value = "unchecked")
public class AutomobileService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(AutomobileService.class);

    private final AutomobileRepositorio automobileRepositorio;

    @Autowired
    public AutomobileService(AutomobileRepositorio automobileRepositorio) {
        this.automobileRepositorio = automobileRepositorio;
    }

    @Transactional
    public DatiAutomobileDto putAutomobile(DatiAutomobileDto dto) {
        log.info("PUT AUTOMOBILE SERVICE");
        Automobile entita = mapper.map(dto, Automobile.class);
        return mapper.map(automobileRepositorio.save(entita), DatiAutomobileDto.class);
    }

    @Transactional
    public List<DatiAutomobileDto> putAutomobili() {
        log.info("PUT AUTOMOBILE SERVICE");
        /* CREO UNA LISTA MOCKATA DI AUTO TRAMITE CONFIGURAZIONE */
        List<DatiAutomobileDto> dto = context.getBean("automobili", ArrayList.class);

        List<DatiAutomobileDto> lista = new ArrayList<>();

        dto.forEach(item -> lista.add(putAutomobile(item)));
        return lista;
    }


}
