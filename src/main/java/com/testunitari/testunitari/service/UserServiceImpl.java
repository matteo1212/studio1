package com.testunitari.testunitari.service;

import com.testunitari.testunitari.entity.RoleTable;
import com.testunitari.testunitari.entity.UserMy;
import com.testunitari.testunitari.repositorio.RoleTableRepositorio;
import com.testunitari.testunitari.repositorio.UserRepositorio;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserDetailsService {

    private static final String USER_NOT_FOUND_MESSAGE = "User with username %s not found";

    private final UserRepositorio userRepositorio;
    private final RoleTableRepositorio roleTableRepositorio;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public UserMy save(UserMy user) {
        log.info("Saving user {} to the database", user.getUsername());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepositorio.save(user);
    }


    @Transactional
    public UserMy addRoleToUser(String username, String roleName) {
        log.info("Adding role {} to user {}", roleName, username);
        Optional<UserMy> userEntityOpt = userRepositorio.findByUsername(username);

        if (userEntityOpt.isPresent()) {
            return null;
        }
        UserMy userEntity = userEntityOpt.get();
        RoleTable roleEntity = roleTableRepositorio.findByName(roleName);
        userEntity.getRoles().add(roleEntity);
        return userEntity;
    }

    public List<UserMy> findAll() {
        return userRepositorio.findAll();
    }

    public UserMy findByUsername(String username) {
        return userRepositorio.findByUsername(username).orElse(null);
    }

    @Transactional
    public void aggiungiRuoliEUtenti() {
        RoleTable role1 = new RoleTable(null, "ROLE_USER");
        RoleTable role2 = new RoleTable(null, "ROLE_ADMIN");
        roleTableRepositorio.save(role1);
        roleTableRepositorio.save(role2);

        UserMy user1;
        UserMy user2;
        Optional<UserMy> user1Opt = userRepositorio.findByUsername("thomas");
        Optional<UserMy> user2Opt = userRepositorio.findByUsername("matteo");

        user1 = user1Opt.orElseGet(() -> new UserMy(null, "thomas", "1234", new HashSet<>()));
        user2 = user2Opt.orElseGet(() -> new UserMy(null, "matteo", "1234", new HashSet<>()));

        this.save(user1);
        this.save(user2);

        Set<RoleTable> roles1 = Collections.singleton(role1);
        Set<RoleTable> roles2 = new HashSet<>();
        roles2.add(role1);
        roles2.add(role2);
        user1.setRoles(roles1);
        user2.setRoles(roles2);
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserMy user = userRepositorio.findByUsername(username).orElse(null);
        if (user == null) {
            String message = String.format(USER_NOT_FOUND_MESSAGE, username);
            log.error(message);
            throw new UsernameNotFoundException(message);
        } else {
            log.debug("User found in the database: {}", username);
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getName()));
            });
            return new User(user.getUsername(), user.getPassword(), authorities);
        }
    }

}
