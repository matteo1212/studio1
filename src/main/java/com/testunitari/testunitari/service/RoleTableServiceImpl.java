package com.testunitari.testunitari.service;

import com.testunitari.testunitari.entity.RoleTable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleTableServiceImpl {

    @Transactional
    public void save(RoleTable role) {
        role.setName(role.getName());
    }
}
