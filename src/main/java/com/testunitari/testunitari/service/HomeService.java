package com.testunitari.testunitari.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class HomeService {

    private static final Logger log = LoggerFactory.getLogger(HomeService.class);

}
