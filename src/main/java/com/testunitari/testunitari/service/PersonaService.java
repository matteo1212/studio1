package com.testunitari.testunitari.service;

import com.testunitari.testunitari.BaseService;
import com.testunitari.testunitari.dto.PersonaDto;
import com.testunitari.testunitari.entity.Automobile;
import com.testunitari.testunitari.entity.Persona;
import com.testunitari.testunitari.repositorio.AutomobileRepositorio;
import com.testunitari.testunitari.repositorio.PersonaRepositorio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@SuppressWarnings(value = "unchecked")
public class PersonaService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(PersonaService.class);

    protected PersonaRepositorio personaRepositorio;
    protected AutomobileRepositorio automobileRepositorio;

    @Autowired
    public PersonaService(PersonaRepositorio personaRepositorio, AutomobileRepositorio automobileRepositorio) {
        this.personaRepositorio = personaRepositorio;
        this.automobileRepositorio = automobileRepositorio;
    }

    public List<PersonaDto> getPersone() {
        log.info("GET PERSONA SERVICE");
        return personaRepositorio.findAll().stream().map(persona -> mapper.map(persona, PersonaDto.class)).collect(Collectors.toList());
    }

    public PersonaDto getPersona(Integer id) {
        log.info("GET PERSONA SERVICE");
        Persona persona = personaRepositorio.findById(id).orElse(null);
        List<Automobile> automobili = automobileRepositorio.findByPersona(persona);

        PersonaDto personaDto = mapper.map(persona, PersonaDto.class);
        personaDto.setAutomobili(mapper.map(automobili, ArrayList.class));

        return personaDto;
    }

    @Transactional
    public PersonaDto putPersona(PersonaDto dto) {
        log.info("PUT PERSONA SERVICE");
        Persona entita = mapper.map(dto, Persona.class);
        return mapper.map(personaRepositorio.save(entita), PersonaDto.class);
    }


}
