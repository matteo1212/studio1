package com.testunitari.testunitari.bean;

import com.testunitari.testunitari.dto.AutomobileDto;
import com.testunitari.testunitari.dto.DatiAutomobileDto;
import com.testunitari.testunitari.dto.PersonaDto;
import com.testunitari.testunitari.entity.Automobile;
import com.testunitari.testunitari.entity.Persona;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackageClasses = Persona.class)
public class BeanAutomobileConfiguration {

    @Bean
    public List<DatiAutomobileDto> automobili() {
        List<DatiAutomobileDto> autos = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            DatiAutomobileDto auto = new DatiAutomobileDto();
            auto.setPersona(new PersonaDto().setFluentIdPersona(1));
            auto.setMarca("Ferrari");
            auto.setAnno(String.valueOf(2021 - i));
            if (i == 0) {
                auto.setModello("Enzo");
            }
            if (i == 1) {
                auto.setModello("FXX");
            }
            if (i == 2) {
                auto.setModello("360");
            }
            autos.add(auto);
        }
        return autos;
    }

}
