package com.testunitari.testunitari.bean;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BeanAppConfiguration {

    @Bean
    public ModelMapper getMapper() {
        ModelMapper mm = new ModelMapper();
        mm.getConfiguration().setPreferNestedProperties(true);
        return mm;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
