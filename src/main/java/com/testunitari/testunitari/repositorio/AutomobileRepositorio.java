package com.testunitari.testunitari.repositorio;

import com.testunitari.testunitari.entity.Automobile;
import com.testunitari.testunitari.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AutomobileRepositorio extends JpaRepository<Automobile, Integer> {

    List<Automobile> findByPersona(Persona persona);
}
