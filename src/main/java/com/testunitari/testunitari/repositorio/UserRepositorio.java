package com.testunitari.testunitari.repositorio;

import com.testunitari.testunitari.entity.UserMy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepositorio extends JpaRepository<UserMy, Long> {

    Optional<UserMy> findByUsername(String username);
}
