package com.testunitari.testunitari.repositorio;

import com.testunitari.testunitari.entity.RoleTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleTableRepositorio extends JpaRepository<RoleTable, Long> {

    RoleTable findByName(String name);
}
