package com.testunitari.testunitari.repositorio;

import com.testunitari.testunitari.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepositorio extends JpaRepository<Persona, Integer> {

    List<Persona> findAllByCognomeStartingWith(String nome);
}
