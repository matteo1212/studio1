package com.testunitari.testunitari.controller;

import com.testunitari.testunitari.dto.PersonaDto;
import com.testunitari.testunitari.service.PersonaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
public class PersonaController {

    private final PersonaService personaService;

    @Autowired
    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }

    private static final Logger log = LoggerFactory.getLogger(PersonaController.class);

    @GetMapping("persona")
    public ResponseEntity<List<PersonaDto>> getPersone() {
        log.info("GET PERSONE CONTROLLER");
        return new ResponseEntity<>(personaService.getPersone(), HttpStatus.OK);
    }

    @GetMapping("persona/{idPersona}")
    public ResponseEntity<PersonaDto> getPersona(@PathVariable Integer idPersona) {
        log.info("GET PERSONA CONTROLLER");
        return new ResponseEntity<>(personaService.getPersona(idPersona), HttpStatus.OK);
    }

    @PutMapping("persona")
    public ResponseEntity<PersonaDto> putPersona(@RequestBody PersonaDto dto) {
        log.info("GET PERSONE CONTROLLER");
        return new ResponseEntity<>(personaService.putPersona(dto), HttpStatus.OK);
    }
}
