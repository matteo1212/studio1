package com.testunitari.testunitari.controller;

import com.testunitari.testunitari.dto.AutomobileDto;
import com.testunitari.testunitari.dto.DatiAutomobileDto;
import com.testunitari.testunitari.service.AutomobileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@Controller
@CrossOrigin
public class AutomobileController {

    private static final Logger log = LoggerFactory.getLogger(AutomobileController.class);

    private final AutomobileService automobileService;

    @Autowired
    public AutomobileController(AutomobileService automobileService) {
        this.automobileService = automobileService;
    }

    @PutMapping("automobileMock")
    public ResponseEntity<List<DatiAutomobileDto>> putAutomobili() {
        return new ResponseEntity<>(automobileService.putAutomobili(), HttpStatus.OK);
    }
}
