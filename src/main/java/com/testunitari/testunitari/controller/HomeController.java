package com.testunitari.testunitari.controller;

import com.testunitari.testunitari.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
@RequiredArgsConstructor
public class HomeController {

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    private final UserServiceImpl userService;
//    private final PersonaRepositorio personaRepositorio;

    @Value("${spring.profiles.active}")
    private String profilo;

    @GetMapping("benvenuto")
    public ResponseEntity<String> getBenvenuto() {
        log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.info("CI SONO CAZZO!!!");
//        Persona persona = new Persona();
//        persona.setNome("Matteo");
//        persona.setCognome("Camillò");
//        persona.setCodiceFiscale("CMLMTT92T12I119J");
//        persona.setDatNascita(new Date());
//        personaRepositorio.save(persona);
        userService.aggiungiRuoliEUtenti();
        log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        return new ResponseEntity<>("Benvenuto Stronzo: " + profilo, HttpStatus.OK);
    }
}
