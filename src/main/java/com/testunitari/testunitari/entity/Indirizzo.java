package com.testunitari.testunitari.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "INDIRIZZO")
//@Where(clause = AuditibleModel.WHERE_BETWEEN_DATE_CLAUSE)
@NamedQuery(name = "Indirizzo.findAll", query = "SELECT t FROM Indirizzo t")
@Getter
@Setter
public class Indirizzo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_INDIRIZZO")
    private Integer idIndirizzo;

    @Column(name = "VIA")
    private String via;

    @Column(name = "CIVICO")
    private String civico;

    // TODO AGGIUNGERE RELAZIONE PERSONA 1 = 1

    public Indirizzo() {
    }


}
