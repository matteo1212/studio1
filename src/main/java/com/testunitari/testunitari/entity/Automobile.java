package com.testunitari.testunitari.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "AUTOMOBILE")
//@Where(clause = AuditibleModel.WHERE_BETWEEN_DATE_CLAUSE)
@NamedQuery(name = "Automobile.findAll", query = "SELECT t FROM Automobile t")
@Getter
@Setter
public class Automobile extends Veicolo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_AUTOMOBILE")
    private Integer idAutomobile;

    @Column(name = "CARBURANTE")
    private String carburante;

    @Column(name = "NUMERO_PORTE")
    private String numeroPorte;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PERSONA")
    private Persona persona;

    public Automobile() {
    }


}
