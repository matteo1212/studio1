package com.testunitari.testunitari.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "USERS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserMy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_UTENTE")
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @ManyToMany(fetch = EAGER)
    @JoinTable(
            name = "USER_ROLE",
            joinColumns = {@JoinColumn(name = "ID_UTENTE")},
            inverseJoinColumns = {@JoinColumn(name = "ID_ROLE")}
    )
    private Collection<RoleTable> roles = new ArrayList<>();

    public UserMy(String username, String password, Set<RoleTable> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}
