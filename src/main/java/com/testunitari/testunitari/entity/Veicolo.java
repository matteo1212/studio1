package com.testunitari.testunitari.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@Getter
@Setter
public class Veicolo {

    private static final long serialVersionUID = 1L;

    @Column(name = "CV")
    private String cv;

    @Column(name = "MARCA")
    private String marca;

    @Column(name = "MODELLO")
    private String modello;

    @Column(name = "COLORE")
    private String colore;

    @Column(name = "ANNO")
    private String anno;

    public Veicolo() {
    }


}
