package com.testunitari.testunitari.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PERSONA")
@NamedQuery(name = "Persona.findAll", query = "SELECT t FROM Persona t")
//@Accessors(fluent = true)
@Getter
@Setter
@NoArgsConstructor
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PERSONA")
    private Integer idPersona;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @Column(name = "COGNOME", nullable = false)
    private String cognome;

    @Column(name = "CODICE_FISCALE", unique = true, nullable = false)
    private String codiceFiscale;

    @Column(name = "DAT_NASCITA", nullable = false)
    private Date datNascita;

}
