package com.testunitari.testunitari;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(
//        exclude = {
//        SecurityAutoConfiguration.class
//        //    ,ManagementWebSecurityAutoConfiguration.class
//}, scanBasePackages = "com.testunitari.testunitari.auth.profile.matteo"
)
public class TestunitariApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestunitariApplication.class, args);
    }

}
