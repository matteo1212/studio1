package com.testunitari.testunitari;

import com.testunitari.testunitari.dto.PersonaDto;
import com.testunitari.testunitari.repositorio.PersonaRepositorio;
import com.testunitari.testunitari.service.PersonaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class PersonaUnitTests {

    @InjectMocks
    private PersonaService personaService;

    @SpyBean
    private PersonaRepositorio personaRepositorio;

//    @Before
//    public void init() {
//        MockitoAnnotations.openMocks(this);
//        personaService = new PersonaService(personaRepositorio, personaMapper);
//    }

    @Test
    public void testPutPersona() {
        PersonaDto dto = new PersonaDto();
        dto.setNome("Gianni");
        dto.setCognome("Sperti");
        dto.setCodiceFiscale("abcdefghilmnopqr");
        dto.setDatNascita(new Date());
        System.out.println("ciao: ");
        System.out.print(personaService.getPersona(1));
        assertEquals(16, dto.getCodiceFiscale().length(), 0);
        dto = personaService.putPersona(dto);
//        assertNotNull(dto);
        System.out.println(dto);
    }

}
